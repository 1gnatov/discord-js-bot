module.exports = {
	name: 'ping',
    description: 'Ping!',
    aliases: ['pung', 'pang'],
	execute(message, args) {
		message.channel.send('Pong.');
	},
};
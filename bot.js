require('dotenv').config();
const fs = require('fs');

// for development purpose create .env file with DISCORD_TOKEN property in it

const Discord = require('discord.js');
const client = new Discord.Client();
client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
    const command = require(`./commands/${file}`);
    client.commands.set(command.name, command);
}

const prefix = '!';

client.once('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.login(process.env.DISCORD_TOKEN);

client.on('message', message => {
    if (!message.content.startsWith(prefix) || message.author.bot) return;
    
    const args = message.content.slice(prefix.length).split(/ +/);
    const commandName = args.shift().toLowerCase();
    
    const command = client.commands.get(commandName) || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

    if (!command) return;
    
    if (command.args && !args.length) {
        let reply = `Ты не добавил аргументы, ${message.author}!`;
        if (command.usage) {
            reply += `\nПравильное использование должно быть таким: \`${prefix}${command.name} ${command.usage}\``;
        }
        
        return message.channel.send(reply);
    }    
    try {
        command.execute(message, args);
    } catch (error) {
        console.error(error);
        message.reply('there was an error trying to execute that command!');
    }
    
    // if (message.content === 'ping') {
    //     client.commands.get('ping').execute(message, args);
    // }
    
    // if (message.content.startsWith(`${prefix}ping`)) {
    //     message.channel.send('Pong.');
    // } else if (message.content.startsWith(`${prefix}beep`)) {
    //     message.channel.send('Boop.');
    // } else if (message.content === `${prefix}server`) {
    //     message.channel.send(`Server name: ${message.guild.name}\nTotal members: ${message.guild.memberCount}`);
    // } else if (message.content === `${prefix}user-info`) {
    //     message.channel.send(`Your username: ${message.author.username}\nYour ID: ${message.author.id}`);
    // } else if (command === 'args-info') {
    //     if (!args.length) {
    //         return message.channel.send(`You didn't provide any arguments, ${message.author}!`);
    //     }
    
    //     message.channel.send(`Command name: ${command}\nArguments: ${args}`);
    // } else if (command === 'kick') {
    //     if (!message.mentions.users.size) {
    //         return message.reply('you need to tag a user in order to kick them!');
    //     }
    //     // grab the "first" mentioned user from the message
    //     // this will return a `User` object, just like `message.author`
    //     const taggedUser = message.mentions.users.first();    
    //     message.channel.send(`You wanted to kick: ${taggedUser.username}`);
    // } else if (command === 'avatar') {
    //     if (!message.mentions.users.size) {
    //         return message.channel.send(`Your avatar: <${message.author.displayAvatarURL({ format: "png", dynamic: true })}>`);
    //     }
    
    //     const avatarList = message.mentions.users.map(user => {
    //         return `${user.username}'s avatar: <${user.displayAvatarURL({ format: "png", dynamic: true })}>`;
    //     });
    
    //     // send the entire array of strings as a message
    //     // by default, discord.js will `.join()` the array with `\n`
    //     message.channel.send(avatarList);
    // }
});